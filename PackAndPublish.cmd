::@echo OFF

if [%1]==[local] goto setlocalNugetServer
set nugetServer=http://nuget.starbucks.net
goto endOfArgs

:setlocalNugetServer
set nugetServer=http://localnuget.starbucks.net

:endOfArgs

echo %nugetServer%
@echo OFF

:: This should be run from the solution folder.
set solutionFolder=%CD%

:: Use date /t and time /t from command line to get format of your date and
:: time; change substring below as needed.

:: This will create a timestamp like yyyy-mm-yy-hh-mm-ss.
set TIMESTAMP=%DATE:~10,4%-%DATE:~4,2%-%DATE:~7,2%-%TIME:~0,2%-%TIME:~3,2%-%TIME:~6,2%

:: Create new directory
set outputDirectory="%solutionFolder%\FraudDataPackages\%TIMESTAMP%"
md %outputDirectory%

set nugetFolder="C:\Tools\NuGet"
set nugetPackOptions=-Symbols -IncludeReferencedProjects -OutputDirectory %outputDirectory%

:: Build Solution

msbuild /t:Rebuild FraudData.sln

:: Pack the projects



cd %solutionFolder%\Starbucks.FraudData.Provider
%nugetFolder%\NuGet.exe pack Starbucks.FraudData.Provider.csproj %nugetPackOptions%

cd %solutionFolder%\Starbucks.FraudData.CyberSource.Provider
%nugetFolder%\NuGet.exe pack Starbucks.FraudData.CyberSource.Provider.csproj %nugetPackOptions%

cd %solutionFolder%\Starbucks.FraudData.CyberSource.Provider.Common
%nugetFolder%\NuGet.exe pack Starbucks.FraudData.CyberSource.Provider.Common.csproj %nugetPackOptions%

cd %solutionFolder%\Starbucks.FraudData.Dal.Common
%nugetFolder%\NuGet.exe pack Starbucks.FraudData.Dal.Common.csproj %nugetPackOptions%

cd %solutionFolder%\Starbucks.FraudData.Dal.Sql
%nugetFolder%\NuGet.exe pack Starbucks.FraudData.Dal.Sql.csproj %nugetPackOptions%

cd %solutionFolder%\Starbucks.FraudData.Provider.Common
%nugetFolder%\NuGet.exe pack Starbucks.FraudData.Provider.Common.csproj %nugetPackOptions%


cd %solutionFolder%\Starbucks.FraudData.PayPal.Provider
%nugetFolder%\NuGet.exe pack Starbucks.FraudData.PayPal.Provider.csproj %nugetPackOptions%

@echo.

set nuGetPushOptions=-ApiKey 734823ED-7DD5-4AB5-8172-A297D09D1551 -src %nugetServer%

:: Publish the packages

cd /d %outputDirectory%
%nugetFolder%\NuGet.exe push *.nupkg %nugetPushOptions%

cd /d %solutionFolder%