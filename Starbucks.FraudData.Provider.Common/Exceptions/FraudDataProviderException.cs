﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.FraudData.Provider.Common.Exceptions
{
    public class FraudDataProviderException : Exception
    {
        public string Code { get; set; }

        public FraudDataProviderException(string code, string message)
            : base(message)
        {
            Code = code;
        }

        public FraudDataProviderException(string code, string message, Exception innerException)
            : base(message, innerException)            
        {
            Code = code;
        }    
    }
}
