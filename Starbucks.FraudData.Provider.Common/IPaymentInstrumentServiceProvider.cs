﻿using Starbucks.FraudData.Proxy.Common;
using Starbucks.FraudData.Proxy.Common.Models;
using System.Threading.Tasks;

namespace Starbucks.FraudData.Provider.Common
{
    public interface IPaymentInstrumentServiceProvider
    {
        Task<IReport> GetOrderDetail(IOrderRequest orderRequest);
    }
}
