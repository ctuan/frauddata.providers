﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Starbucks.FraudData.Provider.Common.Models
{
    public interface IAccount
    {
        /// <summary>
        ///  Unique identifier for a Paypal account
        /// </summary>
        IPaypalAccountIdentifier PaypalAccountId { get; set; }

        /// <summary>
        /// A Partner's view of the user with this paypalAccountID
        /// </summary>
        IPartnerAccount PartnerAccount { get; set; }
    }
}
