﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.FraudData.Provider.Common.Models
{
    public interface IFraudCheckData
    {
        string OrderSource { get; set; }
        string SvcNumber { get; set; }
        DateTime? CardRegistrationDate { get; set; }
        string SvcNumberReloaded { get; set; }
        string CallCenterRepId { get; set; }
        bool IsCardCurrentlyRegisteredToUser { get; set; }
        string ShippingMethod { get; set; }
        bool IsUserSignedIn { get; set; }
        string UserName { get; set; }
        string EmailAddress { get; set; }
        string PaypalPayerId { get; set; }
        string UserId { get; set; }
        int AgeOfAccountSinceCreation { get; set; }
        bool IsPartner { get; set; }
        int BirthDay { get; set; }
        int BirthMonth { get; set; }
        bool eMailSignUp { get; set; }
        bool MailSignUp { get; set; }
        bool TextMessageSignUp { get; set; }
        bool TwitterSignUp { get; set; }
        bool FacebookSignUp { get; set; }
        decimal PurchaserSessionDuration { get; set; }
        string PurchaserIpAddress { get; set; }
        decimal CardBalance { get; set; }
        string Currency { get; set; }
        string Nickname { get; set; }
        bool IsDefaultSvcCard { get; set; }
        int TotalCardsRegistered { get; set; }
        string SessionId { get; set; }
        bool IsAutoreload { get; set; }
        decimal BalancePriorToReload { get; set; }
        string RewardTierLevel { get; set; }
        int? DaysSinceLastReload { get; set; }
        int? TimeSinceLastPOSTransaction { get; set; }
        Dal.Common.Models.IMDDField[] MDDFields { get; set; }
    }
}
