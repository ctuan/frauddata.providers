﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Starbucks.FraudData.Provider.Common.Models
{
    public interface IDgLineItem : ICommonLineItem
    {
        /// <summary>
        /// Common item fields for all types of product.
        /// </summary>
        DgLineItemTypeEnum Type { get; set; }

        int targetAudienceMinAge { get; set; }

        int targetAudienceMaxAge { get; set; }

        DgLineItemLicenseTypeEnum DgLineItemLicenseTypeEnum { get; set; }
    }
}
