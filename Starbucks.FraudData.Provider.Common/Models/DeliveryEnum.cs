﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Starbucks.FraudData.Provider.Common.Models
{
    public enum DeliveryEnum
    {
        INSTANT_DOWNLOAD,
        STREAMING,
        EMAIL_DOWNLOAD,
        SAME_DAY,
        OVERNIGHT,
        STANDARD
    }
}
