﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Starbucks.FraudData.Provider.Common.Models
{
    public interface ITicketingLineItem : ICommonLineItem
    {
        DateTime startDate { get; set; }

        DateTime EndDate { get; set; }

        IBaseAddress Address { get; set; }

        TicketingLineItemTypeEnum Type { get; set; }

        IList<IPartnerAccount> attendees { get; set; }

        bool Subscription { get; set; }
    }
}
