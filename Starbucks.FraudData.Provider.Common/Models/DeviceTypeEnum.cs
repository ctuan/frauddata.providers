﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Starbucks.FraudData.Provider.Common.Models
{
    public enum DeviceTypeEnum
    {
        BROWSER,
        PC_NATIVE,
        MOBILE_NATIVE
    }
}
