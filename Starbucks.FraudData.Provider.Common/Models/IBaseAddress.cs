﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Starbucks.FraudData.Provider.Common.Models
{
    public interface IBaseAddress
    {
        string Line1 { get; set; }

        string Line2 { get; set; }

        string City { get; set; }

        string State { get; set; }

        string PostCode { get; set; }

        string CountryCode { get; set; } 

    }
}
