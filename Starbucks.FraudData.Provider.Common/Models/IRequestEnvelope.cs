﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Starbucks.FraudData.Provider.Common.Models
{
    public interface IRequestEnvelope
    {
        string ErrorLanguage { get; set; }

        object Other { get; set; }
    }
}
