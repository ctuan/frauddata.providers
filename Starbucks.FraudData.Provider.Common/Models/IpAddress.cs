﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Starbucks.FraudData.Provider.Common.Models
{
    public interface IIpAddress
    {
        string IPAddress { get; set; }

        DateTime FirstSeen { get; set; }

        DateTime LastSeen { get; set; }

        int SeenCount { get; set; }

    }
}
