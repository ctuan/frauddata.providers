﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Starbucks.FraudData.Provider.Common.Models
{
    public interface ICommonLineItem
    {
        string Identifier { get; set; }

        decimal ItemPrice { get; set; }

        string CurrencyCode { get; set; }

        int ItemCount { get; set; }

        string Name { get; set; }

        string Category { get; set; }

        string SubCategory { get; set; }

         string Description { get; set; }
    
    }
}
