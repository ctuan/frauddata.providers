﻿using System;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RiskAssessment.Provider;
using RiskAssessment.Provider.Models;
using Starbucks.FraudData.CyberSource.Provider;
using Starbucks.FraudData.Dal.Common;
using Starbucks.FraudData.Dal.Sql;
using Starbucks.FraudData.Dal.Sql.Configuration;
using Starbucks.FraudData.Provider.Common;
using Starbucks.FraudData.Provider.Common.Models;
using Starbucks.Rewards.Dal.LoyaltyService;
using Starbucks.Rewards.Provider;
using Starbucks.Rewards.Provider.Common;
using Starbucks.TransactionHistory.Dal.Common;
using Starbucks.TransactionHistory.Dal.Sql;
using Starbucks.TransactionHistory.Provider;
using Starbucks.TransactionHistory.Provider.Common;
using Starbucks.TransactionHistory.Provider.Configuration;
using System.Threading.Tasks;

namespace Starbucks.FraudData.Provider.Integration.Tests
{
    /// <summary>
    /// Summary description for UnitTest1
    /// </summary>
    [TestClass]
    public class FraudDataPaypalProviderTest
    {
        ITransactionHistoryProvider _transactionHistoryProvider = null;
        ITransactionHistoryDataProvider _transactionHistorydataProvider = null;
        IPayPalFraudCheckData _paypalFraudCheckData = null;
        FraudDataProviderSettings _paypalFraudsettings = null;

        public FraudDataPaypalProviderTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
         //Use TestInitialize to run code before running each test 
         [TestInitialize()]
         public void MyTestInitialize()
         {
             _transactionHistorydataProvider = new TransactionHistorySqlDataProvider("TransactionHistory", 1000,
                                                                                         1000);
             _transactionHistoryProvider =
                    new TransactionHistoryProvider(_transactionHistorydataProvider,
                       ConfigurationManager.GetSection("transactionHistoryProviderSettings") as TransactionHistoryProviderSettings,
                       new LoyaltyDal(),
                       new RewardsCheckSqlDataProvider("TransactionHistory"));

             _paypalFraudsettings = FraudDataProviderSettings.Settings;
             
             _paypalFraudCheckData =
                 new PayPalFraudCheckData(
                     ConfigurationManager.GetSection("fraudDataDalSettings") as FraudDataDalSqlSettingsSection,
                     _transactionHistorydataProvider, _paypalFraudsettings);

         }
        
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void SetTransactionContextTestForGuestUser()
        {
            ITransactionRequest transactionRequest = new TransactionRequest();
            transactionRequest.TrackingId = Guid.NewGuid().ToString();
            IPayPalFraudDataProvider provider = new PayPalRiskAssessmentProvider(_paypalFraudCheckData,
                                                                                 _paypalFraudsettings);
            if (transactionRequest.IpAddress == null)
            {
                transactionRequest.IpAddress = new IpAddress() {IPAddress = "1.1.1.1"};
            }
            //transactionRequest.RequestEnvelope = new RequestEnvelope();
            //transactionRequest.SenderAccount = new Account();
            //transactionRequest.SubOrders = new ISubOrder[0];
            ITransactionResponse response;
            try
            {
                //Guest users pass guid.empty.
                var x = Task.Run(() => provider.TransactionContext("00000000-0000-0000-0000-000000000000", transactionRequest)).Result;
                Assert.AreEqual(Common.Models.AckCode.Success, x.ResponseEnvelope.Ack);

            }
            catch (AggregateException ex) // For break point debugging
            {
                throw;
            }


        }

        [TestMethod]
        public void SetTransactionContextTest()
        {
            ITransactionRequest transactionRequest = new TransactionRequest();
            transactionRequest.TrackingId = Guid.NewGuid().ToString();
            IPayPalFraudDataProvider provider = new PayPalRiskAssessmentProvider(_paypalFraudCheckData,
                                                                                 _paypalFraudsettings);
            if (transactionRequest.IpAddress == null)
            {
                transactionRequest.IpAddress = new IpAddress() { IPAddress = "1.1.1.1" };
            }
            //transactionRequest.RequestEnvelope = new RequestEnvelope();
            //transactionRequest.SenderAccount = new Account();
            //transactionRequest.SubOrders = new ISubOrder[0];
            ITransactionResponse response;
            try
            {
                var x = Task.Run(() => provider.TransactionContext("8805BCC5-C5D0-42ED-81D4-0A6646A4CBA4", transactionRequest)).Result; // test main user :aktest@test.com
                Assert.AreEqual(Common.Models.AckCode.Success, x.ResponseEnvelope.Ack);

            }
            catch (AggregateException ex) // For break point debugging
            {
                throw;
            }


        }
    }
}
