﻿using System;
using System.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Starbucks.FraudData.CyberSource.Provider;
using Starbucks.FraudData.Dal.Common;
using Starbucks.FraudData.Dal.Common.Models;
using Starbucks.FraudData.Dal.Sql;
using Starbucks.FraudData.Provider.Common;
using Starbucks.FraudData.Dal.Sql.Configuration;
using Starbucks.Rewards.Dal.LoyaltyService;
using Starbucks.Rewards.Provider;
using Starbucks.Rewards.Provider.Common;
using Starbucks.TransactionHistory.Provider;
using Starbucks.TransactionHistory.Provider.Common;
using Starbucks.TransactionHistory.Provider.Configuration;
using Starbucks.TransactionHistory.Dal.Sql;

namespace Starbucks.FraudData.Provider.Integration.Tests
{
    [TestClass]
    public class CyberSourceProviderTests
    {
        ICyberSourceFraudDataProvider _cybersourceProvider = null;
        IRewardsProvider _rewardsProvider = null;
        ICyberSourceFraudDataDal _cybersourceFraudDataDal = null;
        ITransactionHistoryProvider _transactionHistoryDataProvider = null;

        [TestInitialize]
        public void TestInitialize()
        {
            var _transactionHistoryDataProvider =
    new TransactionHistoryProvider(new TransactionHistorySqlDataProvider("TransactionHistory", 1000, 1000),
                                   ConfigurationManager.GetSection("transactionHistoryProviderSettings") as TransactionHistoryProviderSettings,
                                   new LoyaltyDal(),
                                   new RewardsCheckSqlDataProvider("TransactionHistory"));
            _rewardsProvider = new RewardsProvider(new LoyaltyDal());

            _cybersourceFraudDataDal =
               new CyberSourceFraudDataDal(ConfigurationManager.GetSection("fraudDataDalSettings") as FraudDataDalSqlSettingsSection);
            _cybersourceProvider =
                new CyberSourceFraudDataProvider(_cybersourceFraudDataDal, _rewardsProvider,
                                                 new TransactionHistorySqlDataProvider("TransactionHistory", 1000, 1000));

        }

        [TestMethod]
        public void GetUserDataForFraudCheckTest()
        {
            //IClientData clientData = new ClientData { UserId = "4804C2AC-2822-42DC-BEE3-A9FAB4D36C75", CardId = "806F7BFD9FD71BA2" }; //Production User

            IClientData clientData = new ClientData { UserId = "84DF4D2F-A9E4-4416-A555-4710E6F66501", CardId = "" }; //Test Main  User

            var fraudCheckData = _cybersourceProvider.GetUserDataForFraudCheck(clientData);
            Assert.IsNotNull(fraudCheckData.EmailAddress);
            Assert.IsNotNull(fraudCheckData.UserName);
        }
    }
}
