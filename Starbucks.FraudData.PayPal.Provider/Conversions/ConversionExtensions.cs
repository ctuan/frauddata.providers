﻿using RiskAssessment.Provider.Models;
using RiskAssessment.TransactionContextProxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using commonModel = Starbucks.FraudData.Provider.Common.Models;
using LineItem = RiskAssessment.Provider.Models.LineItem;

namespace RiskAssessment.Provider
{
    public static class ConversionExtensions
    {
        public static commonModel.ITransactionResponse ToProvider(SetTransactionContextResponse response)
        {
            //map the models
            var transactionResponse = new TransactionResponse();
            transactionResponse.ResponseEnvelope = new Models.ResponseEnvelope();
            transactionResponse.ResponseEnvelope.Ack = response.responseEnvelope.ack.ToProviderAckType();
            transactionResponse.ResponseEnvelope.Build = response.responseEnvelope.build;
            transactionResponse.ResponseEnvelope.CorrelationId = response.responseEnvelope.correlationId;
            transactionResponse.ResponseEnvelope.Timestamp = response.responseEnvelope.timestamp;
            transactionResponse.TrackingId = response.trackingId;
            if (response.ipAddress != null)
            {
                transactionResponse.IpAddress = new Models.IpAddress {IPAddress = response.ipAddress.ipAddress};
            }
            if (response.additionalData != null )
            {
                transactionResponse.AdditionalData = new commonModel.IPair[2];
                transactionResponse.AdditionalData = (from data in response.additionalData
                                                      select new Models.Pair
                                                      {
                                                          Key = data.key,
                                                          Value = data.value
                                                      }).ToArray();
                
            }
            if (response.senderAccount != null)
            {
                transactionResponse.senderAccount = new Models.Account();
                if (response.senderAccount.paypalAccountId != null)
                {
                    transactionResponse.senderAccount.PaypalAccountId = new Models.PaypalAccountIdentifier
                    {
                        PayerId = response.senderAccount.paypalAccountId.payerId
                    };
                }

                if (response.senderAccount.partnerAccount != null)
                {
                    transactionResponse.senderAccount.PartnerAccount = new Models.PartnerAccount
                    {
                        email = response.senderAccount.partnerAccount.email,
                        FirstName = response.senderAccount.partnerAccount.firstName,
                        LastName = response.senderAccount.partnerAccount.lastName,
                        Phone = response.senderAccount.partnerAccount.phone,
                        LastGoodTransactionDate = response.senderAccount.partnerAccount.lastGoodTransactionDate,
                        CreateDate = response.senderAccount.partnerAccount.createDate,
                        TransactionCountThreeMonths = response.senderAccount.partnerAccount.transactionCountThreeMonths
                    };
                }
            }

            if (response.subOrders != null)
            {
                var orders = response.subOrders.ToList().ConvertAll(ConvertToProviderSubOrder);
                transactionResponse.SubOrders = orders.ToArray()[0];
            }
            return transactionResponse;
        }

        public static commonModel.AckCode ToProviderAckType(this TransactionContextProxy.AckCode ack)
        {
            commonModel.AckCode ackProvider;
            Enum.TryParse(ack.ToString(), true, out ackProvider);
            return ackProvider;
        }

        public static TransactionContextProxy.SubOrder ConvertToProxySubOrder(commonModel.ISubOrder order)
        {
            var requestOrder = new TransactionContextProxy.SubOrder { lineItems = new TransactionContextProxy.LineItem[order.LineItems.Length] };
            requestOrder.lineItems = order.LineItems.ToList().ConvertAll(ConvertToProxyLineItem).ToArray();
            return requestOrder;
        }

        public static TransactionContextProxy.LineItem ConvertToProxyLineItem(commonModel.ILineItem input)
        {
            var common = new TransactionContextProxy.CommonLineItem
            {
                currencyCode = input.RetailLineItem.CurrencyCode,
                identifier = input.RetailLineItem.Identifier,
                itemCount = input.RetailLineItem.ItemCount,
                itemPrice = input.RetailLineItem.ItemPrice,
                name = input.RetailLineItem.Name
            };
            var retailLine = new TransactionContextProxy.RetailLineItem {commonLineItem = common};

            var retail = new TransactionContextProxy.LineItem
            {
                Item = retailLine
            };
            return retail;
        }

        public static commonModel.ISubOrder ConvertToProviderSubOrder(TransactionContextProxy.SubOrder order)
        {
            var length = 0;
            if (order == null)
            {
                return null;
            }
            if (order.lineItems != null)
            {

                var requestOrder = new Models.SubOrder {LineItems = new commonModel.ILineItem[order.lineItems.Length]};
                requestOrder.LineItems = order.lineItems.ToList().ConvertAll(ConvertToProviderLineItem).ToArray();
                return requestOrder;
            }
            return new Models.SubOrder();
        }

        public static LineItem ConvertToProviderLineItem(TransactionContextProxy.LineItem input)
        {
            var item = input.Item as TransactionContextProxy.RetailLineItem;
            if (item != null)
            {
                var retail = new LineItem
                {
                    RetailLineItem =
                        new Models.RetailLineItem
                        {
                            CurrencyCode = item.commonLineItem.currencyCode,
                            Identifier = item.commonLineItem.identifier,
                            ItemCount = item.commonLineItem.itemCount,
                            ItemPrice = item.commonLineItem.itemPrice,
                            Name = item.commonLineItem.name
                        }
                };
                return retail;
            }
            return null;
        }

    }
}
