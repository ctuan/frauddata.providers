﻿using Starbucks.FraudData.Provider.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RiskAssessment.Provider.Models
{
    public class Pair : IPair
    {
        public string Key { get; set; }

        public string Value { get; set; }   
  
    }
}
