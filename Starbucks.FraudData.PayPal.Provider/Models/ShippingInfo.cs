﻿using Starbucks.FraudData.Provider.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RiskAssessment.Provider.Models
{
    public class ShippingInfo : IShippingInfo
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public IBaseAddress BaseAddress { get; set; }

        public bool NewAddress { get; set; }

        public DateTime? createdAt { get; set; }

        public bool PreviousChargeback { get; set; }
    }
}
