﻿using Starbucks.FraudData.Provider.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiskAssessment.Provider.Models
{
    public class AdditionalData : IAdditionalData
    {
        public IPair[] Pairs { get; set; }
    }
}
