﻿using Starbucks.FraudData.Provider.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RiskAssessment.Provider.Models
{
    public class PartnerAccount : IPartnerAccount
    {
        public string AccountId { get; set; }

        public string email { get; set; }

        public string Phone { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime DateOfBirth { get; set; }

        public IBaseAddress MailingAddress { get; set; }

        public DateTime CreateDate { get; set; }

        /// <summary>
        /// The last date of a valid transaction. The transaction need not be a PayPal transaction.
        /// </summary>
        public DateTime LastGoodTransactionDate { get; set; }

        /// <summary>
        /// Total transaction count for this partner account.
        /// </summary>
        public int TransactionCountTotal { get; set; }

        /// <summary>
        /// Total chargeback count for this partner account.
        /// </summary>
        public int ChargebackCountTotal { get; set; }

        /// <summary>
        /// Transaction count in the last three months for this partner account.
        /// </summary>
        public int TransactionCountThreeMonths { get; set; }

        /// <summary>
        /// Chargeback count in the last three months for this partner account.
        /// </summary>
        public int ChargebackCountThreeMonths { get; set; }
    }
}
