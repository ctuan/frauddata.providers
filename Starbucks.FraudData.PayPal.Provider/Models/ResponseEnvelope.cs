﻿using Starbucks.FraudData.Provider.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace RiskAssessment.Provider.Models
{
    public class ResponseEnvelope : IResponseEnvelope
    {
        public DateTime Timestamp { get; set; }

        public Starbucks.FraudData.Provider.Common.Models.AckCode Ack { get; set; }

        public string CorrelationId { get; set; }

        public string Build { get; set; }

        public object Other { get; set; }

    }
}
