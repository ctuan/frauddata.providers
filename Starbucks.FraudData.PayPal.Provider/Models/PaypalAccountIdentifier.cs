﻿using Starbucks.FraudData.Provider.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RiskAssessment.Provider.Models
{
    public class PaypalAccountIdentifier : IPaypalAccountIdentifier
    {
        /// <summary>
        /// The PayPal payer ID, which uniquely identifies a PayPal account.
        /// </summary>
        public string PayerId { get; set; }
    }
}
