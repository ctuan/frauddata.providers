﻿using Starbucks.FraudData.Provider.Common.Models;
using System.Runtime.Serialization;

namespace RiskAssessment.Provider.Models
{
    public class TransactionRequest : ITransactionRequest
    {
        /// <summary>
        ///  A request envelope has generic fields that are common to every request. Each operation request contains a request envelope, 
        ///  in addition to the payload that is specific to the operation.
        /// </summary>
        public IRequestEnvelope RequestEnvelope { get; set; }

        /// <summary>
        ///   A unique ID that you specify to track the payment. NOTE: You are responsible for ensuring that the ID is unique.
        /// </summary>
        public string TrackingId { get; set; }

        /// <summary>
        ///  Contains information about the sender's account
        /// </summary>
        public IAccount SenderAccount { get; set; }

        /// <summary>
        /// Contains information about the receiver's account. When the API caller is the receiver, this field is not needed.
        /// </summary>
        public IAccount ReceiverAccount { get; set; }

        /// <summary>
        ///  Order information for this transaction.
        /// </summary>
        public ISubOrder[] SubOrders { get; set; }

        /// <summary>
        ///  Device data.
        /// </summary>
        public IDevice Device { get; set; }

        /// <summary>
        /// IP address for this transaction.
        /// </summary>
        public IIpAddress IpAddress { get; set; }

        /// <summary>
        /// A list of merchant specific data for the transaction.
        /// </summary>
        public IPair[] AdditionalData { get; set; }
    }
}