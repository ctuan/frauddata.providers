﻿using Starbucks.FraudData.Provider.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RiskAssessment.Provider.Models
{
    public class TicketingLineItem : ITicketingLineItem
    {

        public DateTime startDate
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public DateTime EndDate
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public IBaseAddress Address
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public TicketingLineItemTypeEnum Type { get; set; }

        public IList<IPartnerAccount> attendees { get; set; }

        public bool Subscription { get; set; }

        public string Identifier { get; set; }

        public decimal ItemPrice { get; set; }

        public string CurrencyCode { get; set; }

        public int ItemCount { get; set; }

        public string Name { get; set; }
        public string Category { get; set; }

        public string SubCategory { get; set; }

        public string Description { get; set; }
    }
}
