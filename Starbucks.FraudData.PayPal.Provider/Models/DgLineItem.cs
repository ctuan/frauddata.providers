﻿using Starbucks.FraudData.Provider.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RiskAssessment.Provider.Models
{
    public class DgLineItem : IDgLineItem
    {
        /// <summary>
        /// Common item fields for all types of product.
        /// </summary>
        public DgLineItemTypeEnum Type { get; set; }

        public int targetAudienceMinAge { get; set; }

        public int targetAudienceMaxAge { get; set; }

        public DgLineItemLicenseTypeEnum DgLineItemLicenseTypeEnum { get; set; }

        public string Identifier { get; set; }

        public decimal ItemPrice { get; set; }

        public string CurrencyCode { get; set; }

        public int ItemCount { get; set; }

        public string Name { get; set; }

        public string Category { get; set; }

        public string SubCategory { get; set; }

        public string Description { get; set; }
    }
}
