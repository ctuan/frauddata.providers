﻿using Starbucks.FraudData.Provider.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RiskAssessment.Provider.Models
{
    public class TravelLineItem : ITravelLineItem
    {
        TravelLineItemTypeEnum ITravelLineItem.type { get; set; }

        public string Origin { get; set; }

        public string Destination { get; set; }

        public DateTime DepartureDate { get; set; }

        public string Operator { get; set; }

        public string TripIdentifier { get; set; }

        TravelLineItemClassEnum ITravelLineItem.TravelClass { get; set; }

        public IList<IPartnerAccount> Passengers { get; set; }

        TravelLineItemReasonEnum ITravelLineItem.reason { get; set; }

        TravelLineItemBookingTypeEnum ITravelLineItem.BookingType { get; set; }


        public TravelLineItemTypeEnum type { get; set; }

        public TravelLineItemClassEnum TravelClass { get; set; }

        public TravelLineItemReasonEnum reason { get; set; }

        public TravelLineItemBookingTypeEnum BookingType { get; set; }

        public string Identifier { get; set; }

        public decimal ItemPrice { get; set; }

        public string CurrencyCode { get; set; }

        public int ItemCount { get; set; }

        public string Name { get; set; }

        public string Category { get; set; }

        public string SubCategory { get; set; }

        public string Description { get; set; }
    }
}
