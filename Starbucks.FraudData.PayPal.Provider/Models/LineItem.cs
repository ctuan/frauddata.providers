﻿using Starbucks.FraudData.Provider.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RiskAssessment.Provider.Models
{
    public class LineItem : ILineItem
    {
        public IRetailLineItem RetailLineItem { get; set; }

        public IDgLineItem DgLineItem { get; set; }

        public ITicketingLineItem TicketingLineItem { get; set; }

        public ITelcoLineItem TelcoLineItem { get; set; }

        public ITravelLineItem TravelLineItem { get; set; }

        public IHospitalityLineItem HospitalityLineItem { get; set; }

    }
}
