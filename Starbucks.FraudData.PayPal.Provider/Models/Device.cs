﻿using Starbucks.FraudData.Provider.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RiskAssessment.Provider.Models
{
    public class Device : IDevice
    {
        public string Id { get; set; }

        public string LastSeenIpAddress { get; set; }

        public DeviceTypeEnum Type { get; set; }

        public string OperatingSystem { get; set; }

        public string UserAgent { get; set; }

        public DateTime FirstSeen { get; set; }

        public DateTime LastSeen { get; set; }

        public int SeenCount { get; set; }

    }
}
