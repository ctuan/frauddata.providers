﻿using System.Net;
using System.Threading.Tasks;

namespace Starbucks.FraudData.Proxy.Common
{
    public interface IPaymentInstrumentServiceProxy
    {
        Task<string> GetOrderDetailAsync(NetworkCredential credential, string postdata);
    }
}
