﻿using System;
namespace Starbucks.FraudData.Proxy.Common.Models
{
    public interface ILineItem
    {
        string FulfillmentType { get; set; }
        string MerchantProductSKU { get; set; }
        string Number { get; set; }
        string ProductCode { get; set; }
        string ProductName { get; set; }
        string Quantity { get; set; }
        string TaxAmount { get; set; }
        string UnitPrice { get; set; }
    }
}
