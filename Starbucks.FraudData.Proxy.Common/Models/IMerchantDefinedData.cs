﻿using System;
namespace Starbucks.FraudData.Proxy.Common.Models
{
    public interface IMerchantDefinedData
    {
        string Field1 { get; set; }
        string Field10 { get; set; }
        string Field11 { get; set; }
        string Field12 { get; set; }
        string Field13 { get; set; }
        string Field14 { get; set; }
        string Field15 { get; set; }
        string Field16 { get; set; }
        string Field17 { get; set; }
        string Field18 { get; set; }
        string Field19 { get; set; }
        string Field2 { get; set; }
        string Field20 { get; set; }
        string Field3 { get; set; }
        string Field4 { get; set; }
        string Field5 { get; set; }
        string Field6 { get; set; }
        string Field7 { get; set; }
        string Field8 { get; set; }
        string Field9 { get; set; }
    }
}
