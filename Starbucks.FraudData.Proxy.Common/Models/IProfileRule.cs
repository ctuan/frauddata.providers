﻿namespace Starbucks.FraudData.Proxy.Common.Models
{
    public interface IProfileRule
    {
        string RuleDecision { get; set; }
        string RuleName { get; set; }
    }
}
