﻿namespace Starbucks.FraudData.Proxy.Common.Models
{
    public interface IShipTo
    {
        string Phone { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string Address1 { get; set; }
        string Address2 { get; set; }
        string City { get; set; }
        string State { get; set; }
        string Zip { get; set; }
        string CompanyName { get; set; }
        string Country { get; set; }
    }
}
