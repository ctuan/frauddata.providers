﻿using System.Collections.Generic;
namespace Starbucks.FraudData.Proxy.Common.Models
{
    public interface IReport
    {
        string MerchantID { get; set; }
        string Name { get; set; }
        string ReportEndDate { get; set; }
        string ReportStartDate { get; set; }
        IEnumerable<IRequest> Requests { get; set; }
        string Version { get; set; }
    }
}
