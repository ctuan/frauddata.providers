﻿using System.Collections.Generic;
namespace Starbucks.FraudData.Proxy.Common.Models
{
    public interface IProfile
    {
        string Name { get; set; }
        string ProfileDecision { get; set; }
        string ProfileMode { get; set; }
        IEnumerable<IProfileRule> RuleList { get; set; }
    }
}
