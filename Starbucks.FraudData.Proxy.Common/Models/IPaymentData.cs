﻿using System;
namespace Starbucks.FraudData.Proxy.Common.Models
{
    public interface IPaymentData
    {
        string Amount { get; set; }
        string AuthorizationCode { get; set; }
        string AVSResult { get; set; }
        string AVSResultMapped { get; set; }
        string CurrencyCode { get; set; }
        string PaymentProcessor { get; set; }
        string PaymentRequestID { get; set; }
        string TotalTaxAmount { get; set; }
    }
}
