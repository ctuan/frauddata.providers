﻿namespace Starbucks.FraudData.Proxy.Common.Models
{
    public interface IRiskData
    {
        string AppliedAVS { get; set; }
        string AppliedCategoryGift { get; set; }
        string AppliedCategoryTime { get; set; }
        string AppliedHostHedge { get; set; }
        string AppliedThreshold { get; set; }
        string AppliedTimeHedge { get; set; }
        string AppliedVelocityHedge { get; set; }
        string Factors { get; set; }
        string HostSeverity { get; set; }
        string Score { get; set; }
        string TimeLocal { get; set; }
    }
}
