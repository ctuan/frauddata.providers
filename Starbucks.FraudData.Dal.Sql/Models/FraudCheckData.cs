﻿using Starbucks.FraudData.Dal.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.FraudData.Dal.Sql.Models
{
    public class FraudCheckData : IFraudCheckData
    {

        public static  FraudCheckData FromDalIFraudCheckData(Dal.Common.Models.IFraudCheckData userDataForFraudCheck)
        {
            if (userDataForFraudCheck == null)
            {
                return null;
            }

            return  new FraudCheckData
            {
                //UserId = userDataForFraudCheck.UserId,
                //UserName = userDataForFraudCheck.UserName,
                //IsUserSignedIn = userDataForFraudCheck.IsUserSignedIn,
                //AgeOfAccountSinceCreation = userDataForFraudCheck.AgeOfAccountSinceCreation,
                //IsPartner = userDataForFraudCheck.IsPartner,
                //BirthDay = userDataForFraudCheck.BirthDay,
                //BirthMonth = userDataForFraudCheck.BirthMonth,
                //eMailSignUp = userDataForFraudCheck.eMailSignUp,
                //MailSignUp = userDataForFraudCheck.MailSignUp,
                //TextMessageSignUp = userDataForFraudCheck.TextMessageSignUp,
                //FacebookSignUp = userDataForFraudCheck.FacebookSignUp,
                //EmailAddress = userDataForFraudCheck.EmailAddress,
                //CardRegistrationDate = userDataForFraudCheck.CardRegistrationDate,
                //TotalCardsRegistered = userDataForFraudCheck.TotalCardsRegistered,
                //SvcNumberReloaded = userDataForFraudCheck.SvcNumberReloaded,
                //BalancePriorToReload = userDataForFraudCheck.BalancePriorToReload,
                //IsCardCurrentlyRegisteredToUser = userDataForFraudCheck.IsCardCurrentlyRegisteredToUser,
                //OrderSource = userDataForFraudCheck.OrderSource,
                //PaypalPayerId = userDataForFraudCheck.PaypalPayerId,
                //IsAutoreload = userDataForFraudCheck.IsAutoreload,
                //PurchaserIpAddress = userDataForFraudCheck.PurchaserIpAddress,
                //MDDFields = userDataForFraudCheck.MDDFields
            };
        }

        public string OrderSource { get; set; }

        public string SvcNumber { get; set; }

        public DateTime? CardRegistrationDate { get; set; }

        public string SvcNumberReloaded { get; set; }

        public bool IsCardCurrentlyRegisteredToUser { get; set; }

        public bool IsUserSignedIn { get; set; }

        public string UserName { get; set; }

        public string EmailAddress { get; set; }

        public string PaypalPayerId { get; set; }

        public string UserId { get; set; }

        public int AgeOfAccountSinceCreation { get; set; }

        public bool IsPartner { get; set; }

        public int BirthDay { get; set; }

        public int BirthMonth { get; set; }

        public bool eMailSignUp { get; set; }

        public bool MailSignUp { get; set; }

        public bool TextMessageSignUp { get; set; }

        public bool FacebookSignUp { get; set; }

        public string PurchaserIpAddress { get; set; }

        public int TotalCardsRegistered { get; set; }

        public bool IsAutoreload { get; set; }

        public decimal BalancePriorToReload { get; set; }

        public string RewardTierLevel { get; set; }

        public int? DaysSinceLastReload { get; set; }

        public int? TimeSinceLastPOSTransaction { get; set; }

        public IMDDField[] MDDFields { get; set; }
    }
}
