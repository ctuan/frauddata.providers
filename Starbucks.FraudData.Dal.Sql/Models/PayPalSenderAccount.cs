﻿using Starbucks.FraudData.Dal.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.FraudData.Dal.Sql.Models
{
    public class PayPalSenderAccount:IPayPalSenderAccount
    {
        public string PayerId { get; set; }

        public string EmailAddress { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime? CreateDate { get; set; }

        public string PhoneNumber { get; set; }

        public decimal AverageTxnAmount { get; set; }

        public double TxnFrequencyInDays { get; set; }

        public DateTime LastGoodTransactionDate { get; set; }

        public int TransactionCountThreeMonths { get; set; }
    }
}
