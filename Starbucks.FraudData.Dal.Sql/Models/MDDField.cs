﻿using Starbucks.FraudData.Dal.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.FraudData.Dal.Sql.Models
{
    [DataContract(Namespace="http://schemas.datacontract.org/2004/07/Starbucks.FraudData.Dal.Sql.Models")]
    [KnownType(typeof(Starbucks.FraudData.Dal.Sql.Models.MDDField))]
    public class MDDField : IMDDField
    {
        [DataMember]
        public string ID { get; set; }
        [DataMember]
        public string Value { get; set; }
    }
}
