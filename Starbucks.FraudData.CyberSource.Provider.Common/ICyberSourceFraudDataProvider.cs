﻿using Starbucks.FraudData.Dal.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.FraudData.CyberSource.Provider.Common
{
    public interface ICyberSourceFraudDataProvider
    {
        IFraudCheckData GetUserDataForFraudCheck(string userId, int? cardId);
    }
}
