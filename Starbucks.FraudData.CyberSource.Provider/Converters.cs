﻿using Starbucks.FraudData.CyberSource.Provider.Models;
using Starbucks.FraudData.Provider.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.FraudData.CyberSource.Provider
{
    public static class Converters
    {
        public static IFraudCheckData ToApi(this Dal.Common.Models.IFraudCheckData fraudCheckData)
        {
            return new FraudCheckData
            {
                OrderSource = fraudCheckData.OrderSource,
                SvcNumber = fraudCheckData.SvcNumber,
                CardRegistrationDate = fraudCheckData.CardRegistrationDate,
                SvcNumberReloaded = fraudCheckData.SvcNumberReloaded,
                IsCardCurrentlyRegisteredToUser = fraudCheckData.IsCardCurrentlyRegisteredToUser,
                IsUserSignedIn = fraudCheckData.IsUserSignedIn,
                UserName = fraudCheckData.UserName,
                EmailAddress = fraudCheckData.EmailAddress,
                PaypalPayerId = fraudCheckData.PaypalPayerId,
                UserId = fraudCheckData.UserId,
                AgeOfAccountSinceCreation = fraudCheckData.AgeOfAccountSinceCreation,
                IsPartner = fraudCheckData.IsPartner,
                BirthDay = fraudCheckData.BirthDay, 
                BirthMonth = fraudCheckData.BirthMonth,
                MailSignUp = fraudCheckData.MailSignUp,
                eMailSignUp = fraudCheckData.eMailSignUp,
                TextMessageSignUp = fraudCheckData.TextMessageSignUp,
                FacebookSignUp = fraudCheckData.FacebookSignUp,
                PurchaserIpAddress = fraudCheckData.PurchaserIpAddress,
                TotalCardsRegistered = fraudCheckData.TotalCardsRegistered,
                IsAutoreload = fraudCheckData.IsAutoreload,
                BalancePriorToReload = fraudCheckData.BalancePriorToReload,
                RewardTierLevel = fraudCheckData.RewardTierLevel,
                DaysSinceLastReload = fraudCheckData.DaysSinceLastReload,
                TimeSinceLastPOSTransaction = fraudCheckData.TimeSinceLastPOSTransaction,
                MDDFields = fraudCheckData.MDDFields
            };
        }
    }
}
