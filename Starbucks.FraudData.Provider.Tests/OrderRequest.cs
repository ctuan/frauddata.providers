﻿using Starbucks.FraudData.Proxy.Common.Models;

namespace Starbucks.FraudData.Provider.Tests
{
    public class OrderRequest : IOrderRequest
    {
        public string MerchantId { get; set; }

        public string Type { get; set; }

        public string SubType { get; set; }

        public string TargetDate { get; set; }

        public string MerchantReferenceNumber { get; set; }

        public string VersionNumber { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public string RequestId { get; set; }

    }
}
