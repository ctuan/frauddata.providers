﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Starbucks.FraudData.Proxy.Common;
using Starbucks.FraudData.Proxy.Common.Models;
using Starbucks.TestTools.Unit;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace Starbucks.FraudData.Provider.Tests
{
    [TestClass]
    public class PaymentInstrumentServiceProviderTests : TestFor<PaymentInstrumentServiceProvider>
    {
        private DataContracts.Report _report;
        private IReport _resultReport;
        private string _reportXml;
        private DataContracts.BillTo _billTo;
        private DataContracts.LineItem _lineItem;
        private DataContracts.MerchantDefinedData _merchantDefinedData;
        private DataContracts.PaymentData _paymentData;
        private DataContracts.Profile _profile;
        private DataContracts.ProfileRule _profileRule;
        private DataContracts.ShipTo _shipTo;
        private DataContracts.RiskData _riskData;
        private DataContracts.PaymentMethod _paymentMethod;
        private DataContracts.Request _request;
        private DataContracts.ApplicationReply _applicationReply;
        private OrderRequest _OrderRequest;

        [TestInitialize]
        public void TestInitialize()
        {
            GivenApplicationReply();
            GivenBillTo();
            GivenLineItems();
            GivenMerchantDefinedData();
            GivenPaymentData();
            GivenPaymentMethod();
            GivenProfileRule();
            GivenProfile();
            GivenRiskData();
            GivenShipTo();
            GivenRequest();
            GivenReport();
            Serialize();
        }

        [TestMethod]
        public void GetOrderDetail_Should_Return_IReport()
        {
            GivenPaymentInstrumentProxy();
            GivenOrderRequest();

            WhenGetOrderDetailIsInvoked();

            ThenItShouldReturnIReportWithCorrectValues();
        }

        private void ThenItShouldReturnIReportWithCorrectValues()
        {
            Assert.IsNotNull(_resultReport);
            Assert.AreEqual(_report.MerchantID, _resultReport.MerchantID);
            Assert.AreEqual(_report.Name, _resultReport.Name);
            Assert.AreEqual(_report.Requests.Length, _resultReport.Requests.Count());
            Assert.AreEqual(_report.Requests.First().MerchantReferenceNumber, _resultReport.Requests.First().MerchantReferenceNumber);
        }

        private void WhenGetOrderDetailIsInvoked()
        {
            Task<IReport> taskReport = Target.GetOrderDetail(_OrderRequest);

            _resultReport = taskReport.Result;
        }

        private void GivenOrderRequest()
        {

            _OrderRequest = new OrderRequest
            {
                Username = "reload",
                Password = "@dmin!@#",
                MerchantId = "vasdf"
            };
        }

        private void GivenPaymentInstrumentProxy()
        {
            var taskSource = new TaskCompletionSource<string>();
            taskSource.SetResult(_reportXml);
            MockOf<IPaymentInstrumentServiceProxy>().Setup(proxy => proxy.GetOrderDetailAsync(It.IsAny<NetworkCredential>(), It.IsAny<string>())).Returns(taskSource.Task);
        }

        private void Serialize()
        {
            using (var stringWriter = new StringWriter())
            {
                var writer = XmlWriter.Create(stringWriter);
                var serializer = new XmlSerializer(typeof(DataContracts.Report));
                serializer.Serialize(writer, _report);
                _reportXml = stringWriter.ToString();

            }
        }
        
        private void GivenReport()
        {
            _report = new DataContracts.Report
            {
                MerchantID = "SbuxReload",
                Name = "Starbucks",
                ReportEndDate = "10102014",
                ReportStartDate = "10092014",
                Version = "13.3",
                Requests = new[] { _request }
            };


        }

        private void GivenRequest()
        {
            _request = new DataContracts.Request
            {
                ApplicationReplies = new[] { _applicationReply },
                BillTo = _billTo,
                LineItems = new[] { _lineItem },
                MerchantDefinedData = _merchantDefinedData,
                MerchantReferenceNumber = "00SDC9099",
                PaymentData = _paymentData,
                PaymentMethod = new[] { _paymentMethod },
                PredecessorRequestID = "RE234234",
                ProfileList = new[] { _profile },
                RequestDate = "10102012",
                RequestID = "Reasdf999",
                RiskData = _riskData,
                ShipTo = _shipTo,
                Source = "Source",
                SubscriptionID = "SubIDasdf",
                TransactionReferenceNumber = "TraR000#123",
            };
        }

        private void GivenApplicationReply()
        {
            _applicationReply = new DataContracts.ApplicationReply
            {
                Name = "Chaltu",
                RCode = "CodeXPX",
                RFlag = "MDFasdf",
                RMsg = "There is no message.That is the message"
            };
        }

        private void GivenBillTo()
        {
            _billTo = new DataContracts.BillTo
            {
                Address1 = "333 44th ave",
                City = "No Name",
                Country = "Dafha",
                CustomerID = "234523",
                Email = "agasd@asdf.cod",
                FirstName = "Chebude",
                Hostname = "Host@",
                IPAddress = "10.11.11.22",
                LastName = "Tachede",
                Phone = "4533454345",
                State = "Wadiya",
                Zip = "00000",
            };
        }

        private void GivenLineItems()
        {
            _lineItem = new DataContracts.LineItem
            {
                FulfillmentType = "Physical",
                MerchantProductSKU = "435345",
                Number = "324",
                ProductCode = "This is Product code",
                ProductName = "Card",
                Quantity = "4",
                TaxAmount = "32",
                UnitPrice = "45",
            };
        }

        private void GivenMerchantDefinedData()
        {
            _merchantDefinedData = new DataContracts.MerchantDefinedData
            {
                Field1 = " Field1 ",
                Field2 = " Field2 ",
                Field3 = " Field3 ",
                Field4 = " Field4 ",
                Field5 = " Field5 ",
                Field6 = " Field6 ",
                Field7 = " Field7 ",
                Field8 = " Field8 ",
                Field9 = " Field9 ",
                Field10 = " Field10",
                Field11 = " Field11",
                Field12 = " Field12",
                Field13 = " Field13",
                Field14 = " Field14",
                Field15 = " Field15",
                Field16 = " Field16",
                Field17 = " Field17",
                Field18 = " Field18",
                Field19 = " Field19",
                Field20 = " Field20",

            };
        }

        private void GivenPaymentData()
        {
            _paymentData = new DataContracts.PaymentData
            {
                Amount = "10203",
                AuthorizationCode = "34",
                AVSResult = "Some Result",
                AVSResultMapped = "Is mapped",
                CurrencyCode = "USD",
                PaymentProcessor = "Who knows",
                PaymentRequestID = "Req---324",
                TotalTaxAmount = "4",
            };
        }

        private void GivenPaymentMethod()
        {
            _paymentMethod = new DataContracts.PaymentMethod
            {
                AccountSuffix = "This is Prefix",
                CardType = "Mastercard",
                ExpirationMonth = "January",
                ExpirationYear = "2034"
            };
        }

        private void GivenProfile()
        {
            _profile = new DataContracts.Profile
            {
                RuleList = new[] { _profileRule },
                Name = "Chala",
                ProfileDecision = "Some Decision",
                ProfileMode = "A barrell roll"
            };
        }

        private void GivenProfileRule()
        {
            _profileRule = new DataContracts.ProfileRule
            {
                RuleName = "My Rule",
                RuleDecision = "Who Cares"
            };
        }

        private void GivenShipTo()
        {
            _shipTo = new DataContracts.ShipTo
            {
                Phone = "123244443334"
            };
        }

        private void GivenRiskData()
        {
            _riskData = new DataContracts.RiskData
            {
                AppliedAVS = "Whatever",
                AppliedCategoryGift = "AnyThing",
                AppliedCategoryTime = "10:00",
                AppliedHostHedge = "HostHedge",
                AppliedThreshold = "Threshold",
                AppliedTimeHedge = "TimeHedge",
                AppliedVelocityHedge = "VelocityHedge",
                Factors = "Factors",
                HostSeverity = "Host Severity",
                Score = "434",
                TimeLocal = "23:00"
            };
        }

    }
}
