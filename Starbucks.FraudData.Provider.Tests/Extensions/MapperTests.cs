﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Starbucks.FraudData.Provider;
using Starbucks.FraudData.Provider.Extensions;
using Starbucks.FraudData.Proxy.Common.Models;
using System.Linq;

namespace Starbucks.FraudData.Provider.Tests.Extensions
{
    [TestClass]
    public class MapperTests
    {
        private DataContracts.ShipTo _shipTo;
        private DataContracts.RiskData _riskData;
        private DataContracts.ProfileRule _profileRule;
        private DataContracts.Profile _profile;
        private DataContracts.PaymentMethod _paymentMethod;
        private DataContracts.PaymentData _paymentData;
        private DataContracts.MerchantDefinedData _merchantDefinedData;
        private DataContracts.LineItem _lineItem;
        private DataContracts.BillTo _billTo;
        private DataContracts.ApplicationReply _applicationReply;
        private DataContracts.Request _request;
        private DataContracts.Report _report;

        [TestMethod]
        public void ToIShipTo_Should_Correctly_Map()
        {
            GivenShipTo();

            IShipTo iShipTo = _shipTo.ToIShipTo();

            ThenShipToShouldCorrectlyMapToIShipTo(iShipTo);
        }

        [TestMethod]
        public void ToIRiskData_Should_Correctly_Map()
        {
            GivenRiskData();

            IRiskData iRiskData = _riskData.ToIRisKData();

            ThenRiskDataShouldMapToIRiskData(iRiskData);
        }

        [TestMethod]
        public void ToIProfileRule_Should_Correctly_Map()
        {
            GivenProfileRule();

            IProfileRule iProfileRule = _profileRule.ToIProfileRule();

            ThenProfileRuleShouldMapToIProfileRule(iProfileRule);

        }

        [TestMethod]
        public void ToIProfile_Should_Correctly_Map()
        {
            GivenProfileRule();
            GivenProfile();

            IProfile iProfile = _profile.ToIProfile();

            ThenProfileShouldCorrectlyMapToIProfile(iProfile);
            ThenProfileRuleShouldMapToIProfileRule(iProfile.RuleList.First());
        }

        [TestMethod]
        public void ToIPaymentMethod_Should_Correctly_Map()
        {
            GivenPaymentMethod();

            IPaymentMethod iPaymentMethod = _paymentMethod.ToIPaymentMethod();

            ThenPaymentMethodShouldMapToIPaymentMethod(iPaymentMethod);
        }

        [TestMethod]
        public void ToIPaymentData_Should_Correctly_Map()
        {
            GivenPaymentData();

            IPaymentData iPaymentData = _paymentData.ToIPaymentData();

            ThenPaymentDataShouldMapToIPaymentData(iPaymentData);
        }

        [TestMethod]
        public void ToIMerchantDefinedData_Should_Correctly_Map()
        {
            GivenMerchantDefinedData();

            IMerchantDefinedData iMerchantDefinedData = _merchantDefinedData.ToIMerchantDefinedData();

            ThenMerchantDefinedDataShouldMapToIMerchantDefinedData(iMerchantDefinedData);
        }

        [TestMethod]
        public void ToILineItem_Should_Correctly_Map()
        {
            GivenLineItems();

            ILineItem iLineItem = _lineItem.ToILineItem();
            ThenLineItemShouldMapToILineItem(iLineItem);
        }

        [TestMethod]
        public void ToIBillTo_Should_Correctly_Map()
        {
            GivenBillTo();

            IBillTo iBillTo = _billTo.ToIBillTo();

            ThenBillToShouldMapToIBillTo(iBillTo);
        }

        [TestMethod]
        public void ToIApplicationReply_Should_Correctly_Map()
        {
            GivenApplicationReply();

            IApplicationReply iApplicationReply = _applicationReply.ToIApplicationReply();

            ThenApplicationReplyShouldMapToIApplicationReply(iApplicationReply);
        }

        [TestMethod]
        public void ToIRequest_Should_Correctly_Map()
        {
            GivenApplicationReply();
            GivenBillTo();
            GivenLineItems();
            GivenMerchantDefinedData();
            GivenPaymentData();
            GivenPaymentMethod();
            GivenProfileRule();
            GivenProfile();
            GivenRiskData();
            GivenShipTo();
            GivenRequest();

            IRequest iRequest = _request.ToIRequest();

            ThenRequestShouldMapToIRequest(iRequest);
            ThenApplicationReplyShouldMapToIApplicationReply(iRequest.ApplicationReplies.First());
            ThenBillToShouldMapToIBillTo(iRequest.BillTo);
            ThenLineItemShouldMapToILineItem(iRequest.LineItems.First());
            ThenMerchantDefinedDataShouldMapToIMerchantDefinedData(iRequest.MerchantDefinedData);
            ThenPaymentMethodShouldMapToIPaymentMethod(iRequest.PaymentMethod.First());
            ThenPaymentDataShouldMapToIPaymentData(iRequest.PaymentData);
            ThenProfileRuleShouldMapToIProfileRule(iRequest.ProfileList.First().RuleList.First());
            ThenProfileShouldCorrectlyMapToIProfile(iRequest.ProfileList.First());
            ThenRiskDataShouldMapToIRiskData(iRequest.RiskData);
            ThenShipToShouldCorrectlyMapToIShipTo(iRequest.ShipTo);
        }

        [TestMethod]
        public void ToIReport_Should_Correctly_Map()
        {
            GivenApplicationReply();
            GivenBillTo();
            GivenLineItems();
            GivenMerchantDefinedData();
            GivenPaymentData();
            GivenPaymentMethod();
            GivenProfileRule();
            GivenProfile();
            GivenRiskData();
            GivenShipTo();
            GivenRequest();
            GivenReport();

            IReport iReport = _report.ToIReport();

            ThenReportShouldMapToIReport(iReport);
            ThenRequestShouldMapToIRequest(iReport.Requests.First());
        }

        private void GivenReport()
        {
            _report = new DataContracts.Report
            {
                MerchantID = "SbuxReload",
                Name = "Starbucks",
                ReportEndDate = "10102014",
                ReportStartDate = "10092014",
                Version = "13.3",
                Requests = new[] { _request }
            };
        }

        private void GivenRequest()
        {
            _request = new DataContracts.Request
            {
                ApplicationReplies = new[] { _applicationReply },
                BillTo = _billTo,
                LineItems = new[] { _lineItem },
                MerchantDefinedData = _merchantDefinedData,
                MerchantReferenceNumber = "00SDC9099",
                PaymentData = _paymentData,
                PaymentMethod = new[] { _paymentMethod },
                PredecessorRequestID = "RE234234",
                ProfileList = new[] { _profile },
                RequestDate = "10102012",
                RequestID = "Reasdf999",
                RiskData = _riskData,
                ShipTo = _shipTo,
                Source = "Source",
                SubscriptionID = "SubIDasdf",
                TransactionReferenceNumber = "TraR000#123",
            };
        }

        private void GivenApplicationReply()
        {
            _applicationReply = new DataContracts.ApplicationReply
            {
                Name = "Chaltu",
                RCode = "CodeXPX",
                RFlag = "MDFasdf",
                RMsg = "There is no message.That is the message"
            };
        }

        private void GivenBillTo()
        {
            _billTo = new DataContracts.BillTo
            {
                Address1 = "333 44th ave",
                City = "No Name",
                Country = "Dafha",
                CustomerID = "234523",
                Email = "agasd@asdf.cod",
                FirstName = "Chebude",
                Hostname = "Host@",
                IPAddress = "10.11.11.22",
                LastName = "Tachede",
                Phone = "4533454345",
                State = "Wadiya",
                Zip = "00000",
            };
        }

        private void GivenLineItems()
        {
            _lineItem = new DataContracts.LineItem
            {
                FulfillmentType = "Physical",
                MerchantProductSKU = "435345",
                Number = "324",
                ProductCode = "This is Product code",
                ProductName = "Card",
                Quantity = "4",
                TaxAmount = "32",
                UnitPrice = "45",
            };
        }

        private void GivenMerchantDefinedData()
        {
            _merchantDefinedData = new DataContracts.MerchantDefinedData
            {
                Field1 = " Field1 ",
                Field2 = " Field2 ",
                Field3 = " Field3 ",
                Field4 = " Field4 ",
                Field5 = " Field5 ",
                Field6 = " Field6 ",
                Field7 = " Field7 ",
                Field8 = " Field8 ",
                Field9 = " Field9 ",
                Field10 = " Field10",
                Field11 = " Field11",
                Field12 = " Field12",
                Field13 = " Field13",
                Field14 = " Field14",
                Field15 = " Field15",
                Field16 = " Field16",
                Field17 = " Field17",
                Field18 = " Field18",
                Field19 = " Field19",
                Field20 = " Field20",

            };
        }

        private void GivenPaymentData()
        {
            _paymentData = new DataContracts.PaymentData
            {
                Amount = "10203",
                AuthorizationCode = "34",
                AVSResult = "Some Result",
                AVSResultMapped = "Is mapped",
                CurrencyCode = "USD",
                PaymentProcessor = "Who knows",
                PaymentRequestID = "Req---324",
                TotalTaxAmount = "4",
            };
        }

        private void GivenPaymentMethod()
        {
            _paymentMethod = new DataContracts.PaymentMethod
            {
                AccountSuffix = "This is Prefix",
                CardType = "Mastercard",
                ExpirationMonth = "January",
                ExpirationYear = "2034"
            };
        }

        private void GivenProfile()
        {
            _profile = new DataContracts.Profile
            {
                RuleList = new[] { _profileRule },
                Name = "Chala",
                ProfileDecision = "Some Decision",
                ProfileMode = "A barrell roll"
            };
        }

        private void GivenProfileRule()
        {
            _profileRule = new DataContracts.ProfileRule
            {
                RuleName = "My Rule",
                RuleDecision = "Who Cares"
            };
        }

        private void GivenShipTo()
        {
            _shipTo = new DataContracts.ShipTo
            {
                Phone = "123244443334"
            };
        }

        private void GivenRiskData()
        {
            _riskData = new DataContracts.RiskData
            {
                AppliedAVS = "Whatever",
                AppliedCategoryGift = "AnyThing",
                AppliedCategoryTime = "10:00",
                AppliedHostHedge = "HostHedge",
                AppliedThreshold = "Threshold",
                AppliedTimeHedge = "TimeHedge",
                AppliedVelocityHedge = "VelocityHedge",
                Factors = "Factors",
                HostSeverity = "Host Severity",
                Score = "434",
                TimeLocal = "23:00"
            };
        }

        private void ThenReportShouldMapToIReport(IReport iReport)
        {

            Assert.AreEqual(_report.MerchantID, iReport.MerchantID);
            Assert.AreEqual(_report.Name, iReport.Name);
            Assert.AreEqual(_report.ReportEndDate, iReport.ReportEndDate);
            Assert.AreEqual(_report.ReportStartDate, iReport.ReportStartDate);
            Assert.AreEqual(_report.Version, iReport.Version);
        }

        private void ThenRequestShouldMapToIRequest(IRequest iRequest)
        {

            Assert.AreEqual(_request.MerchantReferenceNumber, iRequest.MerchantReferenceNumber);
            Assert.AreEqual(_request.PredecessorRequestID, iRequest.PredecessorRequestID);
            Assert.AreEqual(_request.RequestDate, iRequest.RequestDate);
            Assert.AreEqual(_request.RequestID, iRequest.RequestID);
            Assert.AreEqual(_request.Source, iRequest.Source);
            Assert.AreEqual(_request.SubscriptionID, iRequest.SubscriptionID);
            Assert.AreEqual(_request.TransactionReferenceNumber, iRequest.TransactionReferenceNumber);
        }

        private void ThenLineItemShouldMapToILineItem(ILineItem iLineItem)
        {

            Assert.AreEqual(_lineItem.FulfillmentType, iLineItem.FulfillmentType);
            Assert.AreEqual(_lineItem.MerchantProductSKU, iLineItem.MerchantProductSKU);
            Assert.AreEqual(_lineItem.Number, iLineItem.Number);
            Assert.AreEqual(_lineItem.ProductCode, iLineItem.ProductCode);
            Assert.AreEqual(_lineItem.ProductName, iLineItem.ProductName);
            Assert.AreEqual(_lineItem.Quantity, iLineItem.Quantity);
            Assert.AreEqual(_lineItem.TaxAmount, iLineItem.TaxAmount);
            Assert.AreEqual(_lineItem.UnitPrice, iLineItem.UnitPrice);
        }

        private void ThenBillToShouldMapToIBillTo(IBillTo iBillTo)
        {
            Assert.AreEqual(_billTo.Address1, iBillTo.Address1);
            Assert.AreEqual(_billTo.City, iBillTo.City);
            Assert.AreEqual(_billTo.Country, iBillTo.Country);
            Assert.AreEqual(_billTo.CustomerID, iBillTo.CustomerID);
            Assert.AreEqual(_billTo.Email, iBillTo.Email);
            Assert.AreEqual(_billTo.FirstName, iBillTo.FirstName);
            Assert.AreEqual(_billTo.Hostname, iBillTo.Hostname);
            Assert.AreEqual(_billTo.IPAddress, iBillTo.IPAddress);
            Assert.AreEqual(_billTo.LastName, iBillTo.LastName);
            Assert.AreEqual(_billTo.Phone, iBillTo.Phone);
            Assert.AreEqual(_billTo.State, iBillTo.State);
            Assert.AreEqual(_billTo.Zip, iBillTo.Zip);
        }
        
        private void ThenShipToShouldCorrectlyMapToIShipTo(IShipTo iShipTo)
        {
            Assert.AreEqual(_shipTo.Phone, iShipTo.Phone);
        }
        
        private void ThenApplicationReplyShouldMapToIApplicationReply(IApplicationReply iApplicationReply)
        {
            Assert.AreEqual(_applicationReply.Name, iApplicationReply.Name);
            Assert.AreEqual(_applicationReply.RCode, iApplicationReply.RCode);
            Assert.AreEqual(_applicationReply.RFlag, iApplicationReply.RFlag);
            Assert.AreEqual(_applicationReply.RMsg, iApplicationReply.RMsg);
        }

        private void ThenMerchantDefinedDataShouldMapToIMerchantDefinedData(IMerchantDefinedData iMerchantDefinedData)
        {
            Assert.AreEqual(_merchantDefinedData.Field1, iMerchantDefinedData.Field1);
            Assert.AreEqual(_merchantDefinedData.Field2, iMerchantDefinedData.Field2);
            Assert.AreEqual(_merchantDefinedData.Field3, iMerchantDefinedData.Field3);
            Assert.AreEqual(_merchantDefinedData.Field4, iMerchantDefinedData.Field4);
            Assert.AreEqual(_merchantDefinedData.Field5, iMerchantDefinedData.Field5);
            Assert.AreEqual(_merchantDefinedData.Field6, iMerchantDefinedData.Field6);
            Assert.AreEqual(_merchantDefinedData.Field7, iMerchantDefinedData.Field7);
            Assert.AreEqual(_merchantDefinedData.Field8, iMerchantDefinedData.Field8);
            Assert.AreEqual(_merchantDefinedData.Field9, iMerchantDefinedData.Field9);
            Assert.AreEqual(_merchantDefinedData.Field10, iMerchantDefinedData.Field10);
            Assert.AreEqual(_merchantDefinedData.Field11, iMerchantDefinedData.Field11);
            Assert.AreEqual(_merchantDefinedData.Field12, iMerchantDefinedData.Field12);
            Assert.AreEqual(_merchantDefinedData.Field13, iMerchantDefinedData.Field13);
            Assert.AreEqual(_merchantDefinedData.Field14, iMerchantDefinedData.Field14);
            Assert.AreEqual(_merchantDefinedData.Field15, iMerchantDefinedData.Field15);
            Assert.AreEqual(_merchantDefinedData.Field16, iMerchantDefinedData.Field16);
            Assert.AreEqual(_merchantDefinedData.Field17, iMerchantDefinedData.Field17);
            Assert.AreEqual(_merchantDefinedData.Field18, iMerchantDefinedData.Field18);
            Assert.AreEqual(_merchantDefinedData.Field19, iMerchantDefinedData.Field19);
            Assert.AreEqual(_merchantDefinedData.Field20, iMerchantDefinedData.Field20);
        }

        private void ThenRiskDataShouldMapToIRiskData(IRiskData iRiskData)
        {
            Assert.AreEqual(_riskData.AppliedAVS, iRiskData.AppliedAVS);
            Assert.AreEqual(_riskData.AppliedCategoryGift, iRiskData.AppliedCategoryGift);
            Assert.AreEqual(_riskData.AppliedCategoryTime, iRiskData.AppliedCategoryTime);
            Assert.AreEqual(_riskData.AppliedHostHedge, iRiskData.AppliedHostHedge);
            Assert.AreEqual(_riskData.AppliedThreshold, iRiskData.AppliedThreshold);
            Assert.AreEqual(_riskData.AppliedTimeHedge, iRiskData.AppliedTimeHedge);
            Assert.AreEqual(_riskData.AppliedVelocityHedge, iRiskData.AppliedVelocityHedge);
            Assert.AreEqual(_riskData.Factors, iRiskData.Factors);
            Assert.AreEqual(_riskData.HostSeverity, iRiskData.HostSeverity);
            Assert.AreEqual(_riskData.Score, iRiskData.Score);
            Assert.AreEqual(_riskData.TimeLocal, iRiskData.TimeLocal);
        }

        private void ThenProfileRuleShouldMapToIProfileRule(IProfileRule iProfileRule)
        {
            Assert.AreEqual(_profileRule.RuleName, iProfileRule.RuleName);
            Assert.AreEqual(_profileRule.RuleDecision, iProfileRule.RuleDecision);
        }

        private void ThenProfileShouldCorrectlyMapToIProfile(IProfile iProfile)
        {
            Assert.AreEqual(_profile.Name, iProfile.Name);
            Assert.AreEqual(_profile.ProfileDecision, iProfile.ProfileDecision);
            Assert.AreEqual(_profile.ProfileMode, iProfile.ProfileMode);
            Assert.AreEqual(_profile.RuleList.Length, iProfile.RuleList.Count());
        }

        private void ThenPaymentMethodShouldMapToIPaymentMethod(IPaymentMethod iPaymentMethod)
        {
            Assert.AreEqual(_paymentMethod.AccountSuffix, iPaymentMethod.AccountSuffix);
            Assert.AreEqual(_paymentMethod.CardType, iPaymentMethod.CardType);
            Assert.AreEqual(_paymentMethod.ExpirationMonth, iPaymentMethod.ExpirationMonth);
            Assert.AreEqual(_paymentMethod.ExpirationYear, iPaymentMethod.ExpirationYear);
        }

        private void ThenPaymentDataShouldMapToIPaymentData(IPaymentData iPaymentData)
        {
            Assert.AreEqual(_paymentData.Amount, iPaymentData.Amount);
            Assert.AreEqual(_paymentData.AuthorizationCode, iPaymentData.AuthorizationCode);
            Assert.AreEqual(_paymentData.AVSResult, iPaymentData.AVSResult);
            Assert.AreEqual(_paymentData.AVSResultMapped, iPaymentData.AVSResultMapped);
            Assert.AreEqual(_paymentData.CurrencyCode, iPaymentData.CurrencyCode);
            Assert.AreEqual(_paymentData.PaymentProcessor, iPaymentData.PaymentProcessor);
            Assert.AreEqual(_paymentData.PaymentRequestID, iPaymentData.PaymentRequestID);
            Assert.AreEqual(_paymentData.TotalTaxAmount, iPaymentData.TotalTaxAmount);
        }
    }
}
