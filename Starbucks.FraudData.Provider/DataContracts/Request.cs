﻿using System.Xml.Serialization;

namespace Starbucks.FraudData.Provider.DataContracts
{
    public class Request
    {
        [XmlElement("BillTo")]
        public BillTo BillTo { get; set; }

        [XmlElement("ShipTo")]
        public ShipTo ShipTo { get; set; }

        [XmlArrayItem("Card", typeof(PaymentMethod))]
        public PaymentMethod[] PaymentMethod { get; set; }

        [XmlArrayItem("LineItem", typeof(LineItem))]
        public LineItem[] LineItems { get; set; }

        [XmlArrayItem("ApplicationReply", typeof(ApplicationReply))]
        public ApplicationReply[] ApplicationReplies { get; set; }

        [XmlElement("PaymentData")]
        public PaymentData PaymentData { get; set; }

        [XmlElement("MerchantDefinedData")]
        public MerchantDefinedData MerchantDefinedData { get; set; }

        [XmlElement("RiskData")]
        public RiskData RiskData { get; set; }

        [XmlArrayItem("Profile", typeof(Profile))]
        public Profile[] ProfileList { get; set; }

        [XmlAttribute]
        public string MerchantReferenceNumber { get; set; }

        [XmlAttribute]
        public string RequestDate { get; set; }

        [XmlAttribute]
        public string RequestID { get; set; }

        [XmlAttribute]
        public string SubscriptionID { get; set; }

        [XmlAttribute]
        public string Source { get; set; }

        [XmlAttribute]
        public string TransactionReferenceNumber { get; set; }

        [XmlAttribute]
        public string PredecessorRequestID { get; set; }
    }
}
