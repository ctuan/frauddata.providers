﻿using System.Xml.Serialization;

namespace Starbucks.FraudData.Provider.DataContracts
{
    public class Profile 
    {
        public string ProfileDecision { get; set; }

        public string ProfileMode { get; set; }

        [XmlArrayItemAttribute("Rule", typeof(ProfileRule))]
        public ProfileRule[] RuleList { get; set; }

        [XmlAttribute]
        public string Name { get; set; }
    }
}
