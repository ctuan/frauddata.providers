﻿using System.Xml.Serialization;

namespace Starbucks.FraudData.Provider.DataContracts
{
    public class PaymentData
    {
        public string PaymentRequestID { get; set; }

        public string PaymentProcessor { get; set; }

        public string Amount { get; set; }

        public string CurrencyCode { get; set; }

        public string TotalTaxAmount { get; set; }

        public string AuthorizationCode { get; set; }

        public string AVSResult { get; set; }

        public string AVSResultMapped { get; set; }
    }
}
