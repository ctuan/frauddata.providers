﻿using Starbucks.FraudData.Proxy.Common.Models;

namespace Starbucks.FraudData.Provider.Models
{
    public class ShipTo : IShipTo
    {
        public string Phone { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string CompanyName { get; set; }
        public string Country { get; set; }
    }
}
