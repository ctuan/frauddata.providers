﻿using Starbucks.FraudData.Proxy.Common.Models;

namespace Starbucks.FraudData.Provider.Models
{
    public class PaymentMethod : IPaymentMethod
    {
        public string AccountSuffix { get; set; }

        public string CardType { get; set; }

        public string ExpirationMonth { get; set; }

        public string ExpirationYear { get; set; }
    }
}
