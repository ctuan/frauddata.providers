﻿using Starbucks.FraudData.Provider.Common;
using Starbucks.FraudData.Provider.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.FraudData.Provider
{
    public class FraudDataProvider : IFraudDataProvider
    {
        private readonly ICyberSourceFraudDataProvider _CSFraudDataProvider;

        public FraudDataProvider(ICyberSourceFraudDataProvider CSFraudDataProvider)
        {
            _CSFraudDataProvider = CSFraudDataProvider;
        }

        public IFraudCheckData GetMerchantData(IClientData clientData, MerchantType merchantType)
        {
            switch (merchantType)
            {
                case MerchantType.CyberSource:
                    //var fraudCheckData = _CSFraudDataProvider.GetUserDataForFraudCheck(clientData);
                    //return fraudCheckData.ToApi();
                case MerchantType.CashStar:
                    break;
                case MerchantType.Paypal:
                    break;
            }

            return null;
        }
    }
}
