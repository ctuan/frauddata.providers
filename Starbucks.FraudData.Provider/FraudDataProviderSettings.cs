﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.FraudData.Provider
{
    public class FraudDataProviderSettings : ConfigurationSection
    {
        private static FraudDataProviderSettings _settings;

        public static FraudDataProviderSettings Settings
        {
            get
            {
                return _settings ??
                       (_settings = ConfigurationManager.GetSection("fraudDataProviderSettings") as FraudDataProviderSettings);
            }
        }

        [ConfigurationProperty("payPalSettings", IsRequired = true)]
        public PayPalSettings PayPalSettings
        {
            get { return this["payPalSettings"] as PayPalSettings; }
            set { this["payPalSettings"] = value; }
        }

        [ConfigurationProperty("transactionHistoryPeriodToCybersource")]
        public int TransactionHistoryPeriodToCybersource
        {
            get
            {
                return (int)this["transactionHistoryPeriodToCybersource"];
            }
            set
            {
                this["transactionHistoryPeriodToCybersource"] = value;
            }
        }

        [ConfigurationProperty("transactionHistoryPeriodToPaypal")]
        public int TransactionHistoryPeriodToPaypal
        {
            get
            {
                return (int)this["transactionHistoryPeriodToPaypal"];
            }
            set
            {
                this["transactionHistoryPeriodToPaypal"] = value;
            }
        }
    }
}
